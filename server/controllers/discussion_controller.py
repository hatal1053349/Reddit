from fastapi import APIRouter
from services.discussion_service import get_discussions_in_subreddit, find_search_history_discussions

router = APIRouter(prefix="/discussions")


@router.get("/{subredditName}/{catagory}/{amount}")
def get_discussions(subredditName: str, catagory: str, amount: str) -> list:
    try:
        TRIES = 3
        
        return get_discussions_in_subreddit(subredditName, catagory, amount, TRIES)
    except Exception as e:
        raise e


@router.get("/{search_id}")
def get_search_history_discussions(search_id: int):
    try:
        return find_search_history_discussions(search_id)
    except Exception as e:
        raise e



