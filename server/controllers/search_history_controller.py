from fastapi import APIRouter
from services.search_history_service import find_search_history

router = APIRouter(prefix="/history")


@router.get("")
def get_search_history():
    try:
        return find_search_history()
    except Exception as e:
        raise e


