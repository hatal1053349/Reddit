import datetime
from repositories.search_history_repository import add_search_to_db, get_search_history


def add_search(subreddit: str, catagory: str, creation_date: datetime.datetime = datetime.datetime.now()) -> int:
    try:
        return add_search_to_db(subreddit, catagory, creation_date)
    except Exception as e:
        raise e


def find_search_history() -> list:
    try:
        return get_search_history()
    except Exception as e:
        raise e

