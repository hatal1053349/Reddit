from fastapi import HTTPException
import requests
import datetime
from repositories.discussion_repository import get_discussions, add_discussions_to_db, get_search_history_discussions 
from .search_history_service import add_search


API_URL = "https://api-inference.huggingface.co/models/lxyuan/distilbert-base-multilingual-cased-sentiments-student"
headers = {"Authorization": "Bearer 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"}


def get_discussions_in_subreddit(subredditName: str, catagory: str, amount: str, tries: int = 3) -> list:
    try:
        discussions = get_discussions(subredditName, catagory, amount)
        rated_discussions = []

        for discussion in discussions:
            rated_discussions.append({"title": discussion.title, "id": discussion.id, "rating": add_rating_to_title(discussion.title, tries)})
        
        current_search_id = add_search(subreddit = subredditName, catagory = catagory, creation_date = datetime.datetime.now())
        add_discussions_to_db(current_search_id, rated_discussions)

        return rated_discussions
    
    except HTTPException as e:
        
        raise e
    except Exception as e:

        raise HTTPException(status_code=500, detail=str(e))


def add_rating_to_title(phrase: str, tries) -> dict:
    try:
        response = requests.post(API_URL, headers=headers, json=phrase)

        clean_response = {}

        for rate in response.json()[0]:
            clean_response.update({('%s' % rate["label"]) : rate["score"]})

        return clean_response
    except Exception as e:
        if tries:
            tries -= 1
            return add_rating_to_title(phrase, --tries)
        else: 
            raise e


def find_search_history_discussions(search_id: int) -> list:
    try:
        return get_search_history_discussions(search_id)
    except Exception as e:
        raise e



