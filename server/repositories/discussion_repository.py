from fastapi import HTTPException
import praw
from dbConnection import session
from models.SearchHistory import SearchHistory
from models.Discussion import Discussion


reddit = praw.Reddit(
    client_id="fLQOJWo2yxSWzqLRNr-mHg",
    client_secret="BX--KBwOV1Y7idmaHnYJGrWK03PTCA",
    user_agent="galiNoah",
)


def get_discussions(subredditName: str, catagory: str, amount: str) -> list:
    if isEnglish(subredditName):
        subreddit = reddit.subreddit(subredditName)
        return getattr(subreddit, "%s" % catagory)(limit = int(amount))
    else:
        raise HTTPException(status_code=404, detail="couldn't find subreddit by this name")


def add_discussions_to_db(search_id: str, discussions = []) -> None:
    try:
        discussions_db = []
        for discussion in discussions:
            discussions_db.append(Discussion(discussion_id = discussion["id"], title = discussion["title"], search_id = search_id, positive_rating = discussion["rating"]["positive"], negative_rating = discussion["rating"]["negative"], neutral_rating = discussion["rating"]["neutral"]))
        
        session.add_all(discussions_db)
        session.commit()
    except:
        raise HTTPException(status_code=500, detail="couldn't add discussions to db")

def get_search_history_discussions(search_id: int) -> list:
    try:
        get_all_discussions_query = session.query(Discussion)
        get_discussions_by_id_query = get_all_discussions_query.join(SearchHistory).filter(SearchHistory.search_id == search_id)

        return get_discussions_by_id_query.all()
    except:
        raise HTTPException(status_code=500, detail="couldn't get search histories discussions")
    
def isEnglish(string: str) -> bool:
    try:
        string.encode(encoding='utf-8').decode('ascii')
        return True
    except UnicodeDecodeError:
        return False



