from dbConnection import session
from models.SearchHistory import SearchHistory
import datetime
from fastapi import HTTPException


def add_search_to_db(subreddit: str, catagory: str, creation_date: datetime.datetime = datetime.datetime.now()) -> int:
    try:
        search = SearchHistory(subreddit=subreddit, catagory=catagory, creation_date=creation_date)
            
        session.add(search)
        session.commit()
        
        return search.search_id
    except:
        raise HTTPException(status_code=500, detail="couldn't add search to db")


def get_search_history() -> list:
    try:
        get_all_Searches_query = session.query(SearchHistory)
        
        return get_all_Searches_query.all()
    except:
        raise HTTPException(status_code=500, detail="couldn't get search history")


