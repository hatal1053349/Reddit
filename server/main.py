from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from controllers import discussion_controller, search_history_controller

app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(discussion_controller.router)
app.include_router(search_history_controller.router)




