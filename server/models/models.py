from sqlalchemy.ext.declarative import declarative_base
from dbConnection import engine

Base = declarative_base()

from .Discussion import Discussion 

Base.metadata.create_all(engine)