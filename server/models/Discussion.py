from sqlalchemy import Column, Integer, String, ForeignKey, Numeric
from sqlalchemy.orm import relationship
from .models import Base
from dbConnection import engine


class Discussion(Base):
    __tablename__ = "reddit.discussion"

    discussion_id = Column(String, primary_key=True)
    title = Column(String)
    search_id = Column(Integer, ForeignKey("reddit.search_history.search_id"), primary_key=True)
    positive_rating = Column(Numeric)
    negative_rating = Column(Numeric)
    neutral_rating = Column(Numeric)

    search = relationship("SearchHistory", back_populates="discussions")


Base.metadata.create_all(engine)