from sqlalchemy import Column, Integer, String, DateTime, Enum
from sqlalchemy.orm import relationship
from datetime import datetime
from .models import Base
from dbConnection import engine

Catagory = Enum('hot', 'new', 'top', 'rising', name='catagory')

class SearchHistory(Base):
    __tablename__ = "reddit.search_history"

    search_id = Column(Integer, primary_key=True)
    subreddit = Column(String)
    catagory = Column(Catagory)
    creation_date = Column(DateTime(), default=datetime.now())

    discussions = relationship("Discussion", back_populates="search")

Base.metadata.create_all(engine)    