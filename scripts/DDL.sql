DROP SCHEMA IF EXISTS reddit CASCADE;

CREATE SCHEMA reddit;

DROP TYPE IF EXISTS reddit.catagory CASCADE;

CREATE TYPE reddit.catagory AS ENUM ('Hot', 'New', 'Top', 'Rising');

DROP TABLE IF EXISTS reddit.search_history CASCADE;

CREATE TABLE reddit.search_history
	(
		search_id SERIAL PRIMARY KEY,
		subreddit TEXT,
		catagory reddit.catagory,
		creation_date timestamp default CURRENT_TIMESTAMP
	);
	
DROP TABLE IF EXISTS reddit.discussion;

CREATE TABLE reddit.discussion
	(
		discussion_id TEXT PRIMARY KEY,
		title TEXT,
		search_id INTEGER,
		positive_rating NUMERIC,
		negative_rating NUMERIC,
		neutral_rating NUMERIC,
		CONSTRAINT fk_search
      		FOREIGN KEY(search_id) 
	  		REFERENCES reddit.search_history(search_id) ON DELETE CASCADE ON UPDATE CASCADE
	);

COMMIT;