import axios, { Axios } from 'axios';
import { Catagory } from './enums';
import { DiscussionType, SearchHistoryDiscussionType } from "./types"

const axiosInstance : Axios = axios.create({
    baseURL: 'http://localhost:8000',
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      },
  });

const getDiscussions = async (subreddit: string, catagory: Catagory)=> {
    const AMOUNT = 10;

    return (await axiosInstance.get(`/discussions/${subreddit}/${Catagory[catagory].toLowerCase()}/${AMOUNT}`)).data
}

const getSearchHistory = async () => {
    return (await axiosInstance.get(`/history`)).data
}

const getDiscussionBySearchHistory = async (search_id: number) => {
    const response: SearchHistoryDiscussionType [] =  (await axiosInstance.get(`/discussions/${search_id}`)).data

    return response.map((discussion) => {return {title: discussion.title, id: discussion.discussion_id, rating: {positive: discussion.positive_rating.toString(), neutral: discussion.neutral_rating.toString(), negative: discussion.negative_rating.toString()}}})
}

export {getDiscussions, getSearchHistory, getDiscussionBySearchHistory}