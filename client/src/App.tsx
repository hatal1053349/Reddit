import "./App.css";
import Navbar from "./components/Navbar";
import { useState } from "react";
import {
  getDiscussions,
  getSearchHistory,
  getDiscussionBySearchHistory,
} from "./api";
import { Catagory } from "./enums";
import { DiscussionType, SearchHistoryType } from "./types";
import { AxiosError } from "axios";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HistoryPage from "./pages/HistroyPage";
import MainPage from "./pages/MainPage";

const App: React.FC = () => {
  const [search, setSearch] = useState("");
  const [currentSubreddit, setCurrentSubreddit] = useState("");
  const [filter, setFilter] = useState<Catagory>(Catagory.Hot);
  const [discussions, setDiscussions] = useState<DiscussionType[]>([]);
  const [loading, setLoading] = useState(false);
  const [errMessage, setErrMessage] = useState("");
  const [historyButtonText, setHistoryButtonText] = useState("");
  const [searchHistory, setSearchHistory] = useState<SearchHistoryType[]>([]);
  const [historyDiscussions, setHistoryDiscussions] = useState<
    DiscussionType[]
  >([]);

  const chooseErrorMessage = (status: string): string => {
    if (status === "404") {
      return "oops, couldn't find what you were looking for";
    }

    return "somthing went wrong :< \ntry again later";
  };

  const handleSearchClick = async (currentSearch: string) => {
    setCurrentSubreddit(currentSearch);
    setErrMessage("");
    setDiscussions([]);
    setLoading(true);

    await getDiscussionsBySearch(currentSearch);

    setLoading(false);
  };

  const getDiscussionsBySearch = async (currentSearch: string) => {
    try {
      const discussionsResult = await getDiscussions(currentSearch, filter);
      setDiscussions(discussionsResult);
    } catch (err: unknown) {
      handleError(err);
    }
  };

  const getStatusFromErrorMessage = (err: string): string => {
    return err.slice(-3);
  };

  const changeSearchValue = (currentSearch: string): void => {
    setSearch(currentSearch);
  };

  const handleFilterChange = async (catagory: Catagory) => {
    setFilter(catagory);

    await handleSearchClick(search);
  };

  const handleHistoryButtonClick = async () => {
    try {
      let text: string = "";
      setErrMessage("");

      if (historyButtonText === "") {
        text = "don't";
        const searchHistoryResult = await getSearchHistory();
        setSearchHistory(searchHistoryResult);
      } else {
        setHistoryDiscussions([]);
      }

      setHistoryButtonText(text);
    } catch (err: unknown) {
      handleError(err);
    }
  };

  const showDiscusssionInSearch = async (searchId: number) => {
    try {
        setSearchHistory([]);
        const searchHistoryDiscussionResult =
          await getDiscussionBySearchHistory(searchId);
        setHistoryDiscussions(searchHistoryDiscussionResult);
    } catch (err: unknown) {
      handleError(err);
    }
  };

  const handleError = (err: unknown): void => {
    if (err instanceof AxiosError) {
      setErrMessage(
        chooseErrorMessage(getStatusFromErrorMessage(err.message))
      );
    } else {
      const ERR_STATUS = "400";
      chooseErrorMessage(ERR_STATUS);
    }
  }

  return (
    <body className="App">
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Navbar
                handleSearchClick={handleSearchClick}
                handleHistoryButtonClick={handleHistoryButtonClick}
                historyButtonText={historyButtonText}
                changeSearchValue={changeSearchValue}
              />
            }
          >
            <Route
              index
              element={
                <MainPage
                  discussions={discussions}
                  loading={loading}
                  search={currentSubreddit}
                  handleFilterChange={handleFilterChange}
                  filter={filter}
                />
              }
            />
            <Route
              path="history"
              element={
                <HistoryPage
                  searchHistory={searchHistory}
                  showDiscusssionInSearch={showDiscusssionInSearch}
                  historyDiscussions={historyDiscussions}
                />
              }
            />
          </Route>
        </Routes>
      </BrowserRouter>
      ”
      {errMessage ? (
        <h1 className="d-flex justify-content-center text-danger">
          {errMessage}
        </h1>
      ) : null}
    </body>
  );
};

export default App;
