import React from "react";
import Discussion from "../components/Discussion";
import FilterBar from "../components/FilterBar";
import { Catagory } from "../enums";
import { DiscussionType } from "../types";

interface MainPageProps {
  discussions: DiscussionType[];
  loading: boolean;
  search: string;
  handleFilterChange: (catagory: Catagory) => void;
  filter: Catagory;
}

const MainPage: React.FC<MainPageProps> = ({
  discussions,
  loading,
  search,
  handleFilterChange,
  filter,
}: MainPageProps) => {

  return (
    <div className="row justify-content-center">
      <div className="col-7">
        <FilterBar catagory={filter} handleFilterChange={handleFilterChange} />
        {loading ? (
          <div className="d-flex justify-content-center">
            <div className="spinner-grow text-light m-3" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-light m-3" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-light m-3" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        ) : null}
        <Discussion discussions={discussions} subredditName={search} />
      </div>
    </div>
  );
};

export default MainPage;
