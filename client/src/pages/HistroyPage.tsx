import React from "react";
import Discussion from "../components/Discussion";
import { useState } from "react";
import { DiscussionType, SearchHistoryType } from "../types";
import SearchHistory from "../components/SearchHistory";

interface HistoryPageProps {
  searchHistory: SearchHistoryType[];
  showDiscusssionInSearch: (searchId: number) => void;
  historyDiscussions: DiscussionType[];
}

const HistoryPage: React.FC<HistoryPageProps> = ({
  searchHistory,
  showDiscusssionInSearch,
  historyDiscussions,
}: HistoryPageProps) => {
  
  const [historySubreddit, setHistorySubreddit] = useState("");

  return (
    <div>
      <div className="row justify-content-center">
        <div className="col-7">
          <SearchHistory
            searchHistory={searchHistory}
            showDiscusssionInSearch={showDiscusssionInSearch}
            setHistorySubreddit={setHistorySubreddit}
          />
          <Discussion
            discussions={historyDiscussions}
            subredditName={historySubreddit}
          />
        </div>
      </div>
    </div>
  );
};

export default HistoryPage;
