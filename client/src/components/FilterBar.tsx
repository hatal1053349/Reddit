import React from "react";
import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFire,
  faCertificate,
  faRankingStar,
  faArrowTrendUp,
  IconDefinition,
} from "@fortawesome/free-solid-svg-icons";
import { Catagory } from "../enums";

interface FilterBarProps {
  catagory: Catagory;
  handleFilterChange: (catagory: Catagory) => void;
}

const FilterBar: React.FC<FilterBarProps> = ({
  catagory,
  handleFilterChange,
}: FilterBarProps) => {
  const filterOptionIconMap: IconDefinition[] = [];

  filterOptionIconMap[Catagory.Hot] = faFire;
  filterOptionIconMap[Catagory.New] = faCertificate;
  filterOptionIconMap[Catagory.Top] = faRankingStar;
  filterOptionIconMap[Catagory.Rising] = faArrowTrendUp;

  const checkIfCatagoryChosen = (checkedCatagory: Catagory): boolean => {
    return catagory === checkedCatagory;
  };

  return (
    <div className="FilterBar">
      <div className="card mb-3">
        <div className="row">
          {filterOptionIconMap.map((iconName, catagory) => {
            return (
              <div className="col-3 ">
                <button
                  onClick={() => handleFilterChange(catagory)}
                  className={classnames({
                    "card-body": true,
                    btn: true,
                    "btn-light": true,
                    active: checkIfCatagoryChosen(catagory),
                    "rounded-pill": true,
                  })}
                >
                  <h5 className="card-title">
                    <FontAwesomeIcon icon={iconName} /> {Catagory[catagory]}
                  </h5>
                </button>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default FilterBar;
