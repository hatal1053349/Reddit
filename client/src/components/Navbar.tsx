import React from "react";
import logo from "../Reddit-Logo.png";
import { useState } from "react";
import classnames from "classnames";
import { useRef } from "react";
import { Outlet, Link } from "react-router-dom";

interface NavbarProps {
  handleSearchClick: (currentSearch: string) => void;
  handleHistoryButtonClick: () => void;
  historyButtonText: string;
  changeSearchValue: (currentSearch: string) => void;
}

type userType = {
  username: string;
  profilePictureUrl: string;
  userId: number;
};

const user: userType = {
  username: "random_reddit_user",
  profilePictureUrl: "https://i.redd.it/9ididzjbxtv41.jpg",
  userId: 1,
};

const userImageSize: number = 45;

const Navbar: React.FC<NavbarProps> = ({
  handleSearchClick, handleHistoryButtonClick, historyButtonText, changeSearchValue
}: NavbarProps) => {
  const searchInputRef = useRef(document.createElement("input"));

  const mapHistoryButton = (historyButtonText: string): string =>{
    if(historyButtonText){
      return "/"
    }

    return "/history"
  }

  return (
    <>
    <div className="Navbar mb-3">
      <nav className="navbar navbar-light bg-light">
        <div className="col-1 d-flex justify-content-start">
          <img
            src={logo}
            className="img-fluid rounded-circle"
            alt="subbredit image"
            style={{
              width: 200,
              height: "auto",
              marginTop: "13px"
            }}
          />
          <h4 className="mt-5 mr-0 p-0" style={{color: '#f0401d', fontFamily: 'Sans-Serif', fontSize: '2rem'}}>rater</h4>
        </div>
        <form className="col-6  d-flex mx-5">
          <div className="input-group">
            <input
              className="form-control col-6"
              type="search"
              placeholder="Search reddit"
              aria-label="Search"
              ref={searchInputRef}
              onChange={() => {changeSearchValue(searchInputRef.current.value)}}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-danger"
                type="button"
                onClick={() => {
                  return handleSearchClick(searchInputRef.current.value);
                }}
              >
                Search
              </button>
            </div>
          </div>
        </form>

        <div className="card">
          <div className="row">
            <div className="col-3">
              <img
                src={user.profilePictureUrl}
                className="img-fluid rounded-circle"
                alt="user image"
                style={{
                  width: userImageSize,
                  height: userImageSize,
                }}
              />
            </div>
            <div className="col-9">
              <div className="card-body">
                <p className="card-title">{user.username} </p>
              </div>
            </div>
          </div>
          <Link className={"d-flex align-items-stretch flex-column link-underline link-underline-opacity-0"} to={mapHistoryButton(historyButtonText)}>
          <button
            className={classnames({
              btn: true,
              "btn-outline-dark": true,
              active: historyButtonText,
            })}
            onClick={handleHistoryButtonClick}
          >
            {historyButtonText} show history
          </button>
          </Link>
        </div>
      </nav>
    </div>
    <Outlet/>
    </>
  );
};

export default Navbar;
