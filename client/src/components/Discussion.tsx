import React from "react";
import { DiscussionType } from "../types";

interface DiscussionProps {
  discussions: DiscussionType[];
  subredditName: string;
}

const Discussion: React.FC<DiscussionProps> = ({
  discussions,
  subredditName,
}: DiscussionProps) => {
  const roundRating = (rating: string) => {
    return Math.round(+rating * 10);
  };

  return (
    <div className="Discussion">
      {discussions.map((discussion) => (
        <div className="bg-light text-dark mb-3 rounded " key={discussion.id}>
          <div className="d-flex justify-content-start">
            <h5 className="p-2 m-3 text-muted">r/{subredditName}</h5>
          </div>
          <div className="d-flex justify-content-between">
            <h4 className="m-3">{discussion.title}</h4>
            <div className="col-6 d-flex justify-content-end">
              <h5 className="m-2 text-success">
                {roundRating(discussion.rating.positive)}
              </h5>
              <h5 className="m-2 text-secondary">
                {roundRating(discussion.rating.neutral)}
              </h5>
              <h5 className="m-2 text-danger">
                {roundRating(discussion.rating.negative)}
              </h5>
            </div>
          </div>
        </div>
      ))}
      ;
    </div>
  );
};

export default Discussion;
