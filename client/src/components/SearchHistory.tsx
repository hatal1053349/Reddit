import React from "react";
import { SearchHistoryType } from "../types";

interface SearchHistoryProps {
  searchHistory: SearchHistoryType[];
  showDiscusssionInSearch: (search_id: number) => void;
  setHistorySubreddit: (subreddit: string) => void;
}

const SearchHistory: React.FC<SearchHistoryProps> = ({
  searchHistory,
  showDiscusssionInSearch,
  setHistorySubreddit,
}: SearchHistoryProps) => {
  
  return (
    <div className="SearchHistory">
      {searchHistory.map((search) => (
        <div
          className="bg-light text-dark mb-3 rounded "
          key={search.search_id}
          onClick={() => {showDiscusssionInSearch(search.search_id); setHistorySubreddit(search.subreddit)}}
        >
          <div className="d-flex justify-content-start">
            <h5 className="p-2 m-3 text-muted">
              r/{search.subreddit} {search.catagory}
            </h5>
          </div>
          <div className="d-flex justify-content-end">
            <div className="col-6 d-flex justify-content-end">
              <h5 className="m-2 text-muted">
                {new Date(search.creation_date).toLocaleString()}
              </h5>
            </div>
          </div>
        </div>
      ))}
      ;
    </div>
  );
};

export default SearchHistory;
