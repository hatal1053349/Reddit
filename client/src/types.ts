import Discussion from "./components/Discussion";

type DiscussionType = {
  title: string;
  id: string;
  rating: {
    positive: string;
    negative: string;
    neutral: string;
  };
};

type SearchHistoryType = {
  search_id: number;
  subreddit: string;
  catagory: string;
  creation_date: Date;
};

type SearchHistoryDiscussionType = {
  search_id: number;
  discussion_id: string;
  title: string;
  positive_rating: number;
  neutral_rating: number;
  negative_rating: number;
};

export type { DiscussionType, SearchHistoryType, SearchHistoryDiscussionType };
